# kotlin-mybatis-generator

## Which files does the generator create
1. XxxMapper.xml
1. XxxMapper.kt
1. XxxMapperTest.kt
1. MyMapper.kt

The `MyMapper.kt` is the parent of mappers, it holds the common methods.

```kotlin
import org.springframework.transaction.annotation.Transactional

interface MyMapper<T> {
    fun findOne(id: Long): T
    fun findAll(): List<T>

    @Transactional
    fun insert(entity: T): Int

    @Transactional
    fun insertBatch(list: List<T>)

    @Transactional
    fun update(entity: T): Int

    @Transactional
    fun delete(id: Long): Int

    @Transactional
    fun updateWithoutNull(entity: T): Int

    @Transactional
    fun insertWithoutNull(entity: T): Int
}
```

## Where is generated files
This generator uses the `./generator` as top main generated directory, and it's a maven structure, so you can just copy the generated `src` directory to your main project.

## How to use

### maven

```xml
<dependencies>
    <dependency>
        <groupId>club.wefine.mybatis</groupId>
        <artifactId>kotlin-mybatis-generator</artifactId>
        <version>1.0.1</version>
        <scope>test</scope>
    </dependency>
</dependencies>
```

### kotlin
```kotlin
import club.wefine.mybatis.Generators

fun main(args: Array<String>) {
    val xmlConfig = "./config/mybatis-generator.xml"

    Generators.run(xmlConfig)
}
```
