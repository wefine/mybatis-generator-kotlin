package club.wefine.mybatis

import org.junit.Test
import java.io.File

class GeneratorTest {

    @Test
    fun testGenerator() {
        val xmlConfig = File(ClassLoader.getSystemResource("config/mybatis-generator.xml").file)
        Generators.run(xmlConfig.absolutePath)
    }
}