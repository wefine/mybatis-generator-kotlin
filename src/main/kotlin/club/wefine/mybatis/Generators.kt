package club.wefine.mybatis

import club.wefine.mybatis.cmd.Generator
import club.wefine.mybatis.cmd.impl.*
import org.mybatis.generator.api.MyBatisGenerator
import org.mybatis.generator.api.dom.java.TopLevelClass
import org.mybatis.generator.config.xml.ConfigurationParser
import org.mybatis.generator.internal.DefaultShellCallback
import org.slf4j.LoggerFactory
import java.io.File
import java.util.stream.Collectors

object Generators {
    private val logger = LoggerFactory.getLogger(javaClass)

    fun run(xmlConfig: String) {
        val configFile = File(xmlConfig)
        if (!configFile.isFile) {
            logger.error("Cannot find out xml configuration, path=$xmlConfig!")
            System.exit(1)
        }

        val generators = arrayListOf<Generator>()

        configFile.inputStream().use { it ->
            val config = ConfigurationParser(null).parseConfiguration(it)
            val generator = MyBatisGenerator(config, DefaultShellCallback(true), null)
            generator.generate(null, null, null, false)

            val collect = generator.generatedJavaFiles
                    .stream()
                    .collect(Collectors.partitioningBy { it.compilationUnit is TopLevelClass })

            generators.add(EntityGenerator(collect.getValue(true)))
            generators.add(MapperDaoGenerator(collect.getValue(false)))
            generators.add(ServiceGenerator(collect.getValue(false), collect.getValue(true)))
            generators.add(ControllerGenerator(collect.getValue(false), collect.getValue(true)))
            generators.add(MyMapperGenerator(collect.getValue(false)))
            generators.add(MyServiceGenerator(collect.getValue(false)))
            generators.add(MapperUtGenerator(collect.getValue(false)))

            generators.add(XmlGenerator(generator.generatedXmlFiles))
        }

        generators.forEach { it.execute() }
    }
}