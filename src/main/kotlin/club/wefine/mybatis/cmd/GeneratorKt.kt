package club.wefine.mybatis.cmd

import org.mybatis.generator.api.GeneratedJavaFile

interface GeneratorKt : Generator {
    var gjfs : List<GeneratedJavaFile>
}