package club.wefine.mybatis.cmd

import org.mybatis.generator.api.GeneratedFile
import java.io.File

interface Generator {
    fun execute()

    fun writeFile(content: String, filePath: String) {
        val targetFile = File(filePath)
        if (!targetFile.parentFile.exists()) {
            targetFile.parentFile.mkdirs()
        }

        targetFile.writeText(content)
    }

    fun getFilePath(gf: GeneratedFile, type: GenType = GenType.XML): String {
        val name = gf.fileName.replace(oldValue = ".java", newValue = ".kt")
        val relativePath = gf.targetPackage.replace(oldChar = '.', newChar = '/')
        val prefix = "./generated/src"

        return when (type) {
            GenType.XML -> "$prefix/main/resources/$relativePath/$name"
            GenType.UT -> "$prefix/test/kotlin/$relativePath/$name"
            else -> "$prefix/main/kotlin/$relativePath/$name"
        }
    }

    enum class GenType {
        XML, UT, KT
    }
}