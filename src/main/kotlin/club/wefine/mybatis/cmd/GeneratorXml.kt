package club.wefine.mybatis.cmd

import org.mybatis.generator.api.GeneratedXmlFile

interface GeneratorXml : Generator {
    var gxfs: List<GeneratedXmlFile>
}