package club.wefine.mybatis.cmd.impl

import club.wefine.mybatis.cmd.Generator
import club.wefine.mybatis.cmd.GeneratorXml
import org.jsoup.Jsoup
import org.jsoup.parser.Parser
import org.mybatis.generator.api.GeneratedXmlFile
import org.slf4j.LoggerFactory
import org.jsoup.parser.ParseSettings



class XmlGenerator(override var gxfs: List<GeneratedXmlFile>) : GeneratorXml {
    private val logger = LoggerFactory.getLogger(javaClass)

    override fun execute() {
        logger.info("generate xml files...")

        gxfs.forEach {
            singleWriter(it)
        }

        logger.info("generate xml files...Done!\n")
    }

    private fun singleWriter(gxf: GeneratedXmlFile) {
        var changedIdContent = gxf.formattedContent
        mapOf(
                "updateByPrimaryKey" to "update",
                "selectByPrimaryKey" to "findOne",
                "updateSelective" to "updateWithoutNull",
                "insertSelective" to "insertWithoutNull",
                "deleteByPrimaryKey" to "delete"
        ).forEach { t, u -> changedIdContent = changedIdContent.replace(oldValue = t, newValue = u) }

        val parser = Parser.xmlParser()
        parser.settings(ParseSettings(true, true))

        val jd = Jsoup.parse(changedIdContent, "UTF-8", parser)
        jd.outputSettings().apply {
            prettyPrint(true)
            outline(true)
            indentAmount(4)
        }

        val findOne = jd.getElementById("findOne")

        val tbl = """from (\w+) where""".toRegex().find(findOne.text())!!.groupValues.last()
        val findAll = """
<select id="findAll" resultMap="BaseResultMap">
    select * from $tbl
</select>
        """
        findOne.after(findAll)
        jd.getElementById("findAll").attr("resultMap", "BaseResultMap")

        val insert = jd.getElementById("insert")
        val (insertHead, insertValue) = insert.textNodes().last().text().split("values")
        val insertBatch = """
<insert id="insertBatch" parameterType="java.util.List">
    ${insertHead.trim()} values
    <foreach collection="list" item="item" index="index" separator=",">
        ${insertValue.replace(oldValue = "#{", newValue = "#{item.")}
    </foreach>
</insert>
        """.trim()
        insert.after(insertBatch)
        jd.getElementById("insertBatch").attr("parameterType", "java.util.List")

        writeFile(jd.html(), getFilePath(gxf, Generator.GenType.XML))
    }
}