package club.wefine.mybatis.cmd.impl

import club.wefine.mybatis.cmd.Generator
import club.wefine.mybatis.cmd.GeneratorKt
import org.mybatis.generator.api.GeneratedJavaFile
import org.slf4j.LoggerFactory

class MapperDaoGenerator(override var gjfs: List<GeneratedJavaFile>) : GeneratorKt {
    private val logger = LoggerFactory.getLogger(javaClass)

    override fun execute() {
        logger.info("generate mapper dao files...")

        gjfs.forEach {
            singleWriter(it)
        }

        logger.info("generate mapper dao files...Done\n")
    }

    private fun singleWriter(gjf: GeneratedJavaFile) {
        val clazzName = gjf.compilationUnit.type.shortName
        val entityName = clazzName.replace(regex = "Mapper".toRegex(), replacement = "")
        val importedTypes = gjf.compilationUnit.importedTypes

        val body = """
package ${gjf.targetPackage}

${importedTypes.filter { type -> !type.fullyQualifiedName.startsWith("java.lang") }
                .joinToString(separator = "\n") { type -> "import $type" }}
import org.apache.ibatis.annotations.Mapper

@Mapper
interface $clazzName : MyMapper<$entityName>
""".trim()

        writeFile(body, getFilePath(gjf, Generator.GenType.KT))
    }
}