package club.wefine.mybatis.cmd.impl

import club.wefine.mybatis.cmd.Generator
import club.wefine.mybatis.cmd.GeneratorKt
import org.mybatis.generator.api.GeneratedJavaFile
import org.slf4j.LoggerFactory
import java.io.File

class ServiceGenerator(
        override var gjfs: List<GeneratedJavaFile>,
        var entities: List<GeneratedJavaFile>
) : GeneratorKt {

    private val logger = LoggerFactory.getLogger(javaClass)

    override fun execute() {
        logger.info("generate service files...")
        if (!entities.isEmpty()) {
            val entityPackage = entities[0].targetPackage
            gjfs.forEach {
                singleWriter(it, entityPackage)
            }
        }


        logger.info("generate service files...Done\n")
    }

    private fun singleWriter(gjf: GeneratedJavaFile, entityPackage: String) {
        val mapperPackage = gjf.targetPackage
        val servicePackage = mapperPackage.replace(oldValue = ".mapper", newValue = ".service")

        val clazzName = gjf.compilationUnit.type.shortName
        val entityName = clazzName.replace(regex = "Mapper".toRegex(), replacement = "")
        val serviceName = "${entityName}Service"
        val body = """
package $servicePackage

import $entityPackage.$entityName
import $mapperPackage.MyMapper
import $mapperPackage.${entityName}Mapper
import org.springframework.stereotype.Service
import javax.annotation.Resource

@Service
class $serviceName : MyService<$entityName> {
    @Resource
    lateinit var mapper: ${entityName}Mapper

    override fun getMapper(): MyMapper<$entityName> {
        return mapper
    }
}

""".trim()

        val filePath = getFilePath(gjf, Generator.GenType.KT)
        writeFile(body, "${File(filePath).parentFile.parent}/service/$serviceName.kt")
    }
}