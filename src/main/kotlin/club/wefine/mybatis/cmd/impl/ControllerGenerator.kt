package club.wefine.mybatis.cmd.impl

import club.wefine.mybatis.cmd.Generator
import club.wefine.mybatis.cmd.GeneratorKt
import org.mybatis.generator.api.GeneratedJavaFile
import org.slf4j.LoggerFactory
import java.io.File

class ControllerGenerator(
        override var gjfs: List<GeneratedJavaFile>,
        var entities: List<GeneratedJavaFile>
) : GeneratorKt {

    private val logger = LoggerFactory.getLogger(javaClass)

    override fun execute() {
        logger.info("generate controller files...")
        if (!entities.isEmpty()) {
            val entityPackage = entities[0].targetPackage
            gjfs.forEach {
                singleWriter(it, entityPackage)
            }
        }


        logger.info("generate controller files...Done\n")
    }

    private fun singleWriter(gjf: GeneratedJavaFile, entityPackage: String) {
        val mapperPackage = gjf.targetPackage
        val controllerPackage = mapperPackage.replace(oldValue = ".mapper", newValue = ".controller")
        val servicePackage = mapperPackage.replace(oldValue = ".mapper", newValue = ".service")

        val clazzName = gjf.compilationUnit.type.shortName
        val entityName = clazzName.replace(regex = "Mapper".toRegex(), replacement = "")
        val requestPrefix = "${entityName.toLowerCase()}s"
        val serviceName = "${entityName}Service"
        val controllerName = "${entityName}Controller"
        val body = """
package $controllerPackage


import $entityPackage.$entityName
import $servicePackage.$serviceName
import org.springframework.data.domain.Pageable
import org.springframework.data.web.PageableDefault
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.*
import javax.annotation.Resource

@Controller
class $controllerName {

    @Resource
    lateinit var service: $serviceName

    @GetMapping("$requestPrefix")
    fun list(model: Model): String {
        return "$requestPrefix/list"
    }

    @GetMapping("$requestPrefix/a")
    fun add(model: Model): String {
        return "$requestPrefix/add"
    }

    @GetMapping("$requestPrefix/v/{id}")
    fun view(@PathVariable id: Long, model: Model): String {
        return "$requestPrefix/view"
    }

    // 以下为 REST 接口
    @GetMapping("api/$requestPrefix/{id:\\d+}")
    fun findOne(@PathVariable id: Long): $entityName {
        return service.findOne(id)
    }

    @GetMapping("api/$requestPrefix")
    @ResponseBody
    fun findAll(): List<$entityName> {
        return service.findAll()
    }

    @PostMapping("api/$requestPrefix/query")
    @ResponseBody
    fun pageQuery(@PageableDefault(size = 10, page = 1) pageable: Pageable): List<$entityName> {
        return service.pageQuery(pageable)
    }

    @PostMapping("api/$requestPrefix")
    @ResponseBody
    fun insert(entity: $entityName): Int {
        return service.insert(entity)
    }

    @PutMapping("api/$requestPrefix")
    @ResponseBody
    fun update(entity: $entityName): Int {
        return service.update(entity)
    }

    @DeleteMapping("api/$requestPrefix/{id:\\d+}")
    @ResponseBody
    fun delete(@PathVariable id: Long): Int {
        return service.delete(id)
    }
}
""".trim()

        val filePath = getFilePath(gjf, Generator.GenType.KT)
        writeFile(body, "${File(filePath).parentFile.parent}/controller/$controllerName.kt")
    }
}