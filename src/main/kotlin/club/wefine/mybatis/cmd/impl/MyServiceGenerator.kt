package club.wefine.mybatis.cmd.impl

import club.wefine.mybatis.cmd.Generator
import club.wefine.mybatis.cmd.GeneratorKt
import org.mybatis.generator.api.GeneratedJavaFile
import org.slf4j.LoggerFactory
import java.io.File

class MyServiceGenerator(override var gjfs: List<GeneratedJavaFile>) : GeneratorKt {
    private val logger = LoggerFactory.getLogger(javaClass)

    override fun execute() {
        logger.info("generate MyService file...")
        if(gjfs.isNotEmpty()){
            singleWriter(gjfs[0])
        }
        logger.info("generate MyService file...Done!\n")
    }

    private fun singleWriter(gjf: GeneratedJavaFile) {
        val mapperPackage = gjf.targetPackage
        val servicePackage = mapperPackage.replace(oldValue = ".mapper", newValue = ".service")
        val body = """
package $servicePackage

import $mapperPackage.MyMapper
import com.github.pagehelper.PageHelper
import org.springframework.data.domain.Pageable
import org.springframework.transaction.annotation.Transactional

interface MyService<T> {
    fun getMapper(): MyMapper<T>

    fun findOne(id: Long): T {
        return getMapper().findOne(id)
    }

    fun findAll(): List<T> {
        return getMapper().findAll()
    }

    @Transactional
    fun insert(entity: T): Int {
        return getMapper().insert(entity)
    }

    @Transactional
    fun insertBatch(list: List<T>) {
        getMapper().insertBatch(list)
    }

    @Transactional
    fun update(entity: T): Int {
        return getMapper().update(entity)
    }

    @Transactional
    fun delete(id: Long): Int {
        return getMapper().delete(id)
    }

    @Transactional
    fun updateWithoutNull(entity: T): Int {
        return getMapper().updateWithoutNull(entity)
    }

    @Transactional
    fun insertWithoutNull(entity: T): Int {
        return getMapper().insertWithoutNull(entity)
    }

    fun pageQuery(pageable: Pageable): List<T> {
        PageHelper.startPage<T>(pageable.pageNumber, pageable.pageSize)

        return findAll()
    }
}
""".trim()
        val filePath = getFilePath(gjf, Generator.GenType.KT)
        writeFile(body, "${File(filePath).parentFile.parent}/service/MyService.kt")
    }
}