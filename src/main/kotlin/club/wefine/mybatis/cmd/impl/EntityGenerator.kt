package club.wefine.mybatis.cmd.impl

import club.wefine.mybatis.cmd.Generator
import club.wefine.mybatis.cmd.GeneratorKt
import org.mybatis.generator.api.GeneratedJavaFile
import org.mybatis.generator.api.dom.java.TopLevelClass
import org.slf4j.LoggerFactory

class EntityGenerator(override var gjfs: List<GeneratedJavaFile>) : GeneratorKt {
    private val logger = LoggerFactory.getLogger(javaClass)

    override fun execute() {
        logger.info("generate entity files...")

        gjfs.forEach {
            singleWriter(it)
        }

        logger.info("generate entity files...Done!\n")
    }

    private fun singleWriter(gjf: GeneratedJavaFile) {
        val clazz = gjf.compilationUnit as TopLevelClass

        val body = """
package ${gjf.targetPackage}

${clazz.importedTypes.filter { type -> !type.fullyQualifiedName.startsWith("java.lang") }
                .joinToString(separator = "\n") { type -> "import $type" }}

data class ${clazz.type.shortName} (
${clazz.fields.joinToString(separator = ",\n") { field ->
            "\tvar ${field.name} : ${field.type.shortName}? = null"
        }}
)
""".trim()
        writeFile(body, getFilePath(gjf, Generator.GenType.KT))
    }
}