package club.wefine.mybatis.cmd.impl

import club.wefine.mybatis.cmd.Generator
import club.wefine.mybatis.cmd.GeneratorKt
import org.mybatis.generator.api.GeneratedJavaFile
import org.slf4j.LoggerFactory
import java.io.File

class MapperUtGenerator(override var gjfs: List<GeneratedJavaFile>) : GeneratorKt {
    private val logger = LoggerFactory.getLogger(javaClass)

    override fun execute() {
        logger.info("generate unit test files...")

        gjfs.forEach { singleWriter(it) }

        logger.info("generate unit test files...Done\n")
    }

    private fun singleWriter(gjf: GeneratedJavaFile) {
        val mapperName = gjf.compilationUnit.type.shortName
        val clazzName = "${mapperName}Test"
        val body = """
package ${gjf.targetPackage}

import org.junit.Assert.assertEquals
import org.junit.Test
import org.junit.runner.RunWith
import org.slf4j.LoggerFactory
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringRunner
import javax.annotation.Resource

@RunWith(SpringRunner::class)
@SpringBootTest
class $clazzName {
    private val logger = LoggerFactory.getLogger(javaClass)

    @Resource lateinit var mapper: $mapperName

    @Test
    fun test_01_insert() {
        logger.info("insert")
        assertEquals(1,1)
    }

    @Test
    fun test_01_insertBatch() {
        logger.info("insertBatch")
    }

    @Test
    fun test_01_insertWithoutNull() {
        logger.info("insertWithoutNull")
    }

    @Test
    fun test_02_update() {
        logger.info("update")
    }

    @Test
    fun test_02_updateWithoutNull() {
        logger.info("updateWithoutNull")
    }

    @Test
    fun test_03_delete() {
        logger.info("delete")
    }

    @Test
    fun test_04_findAll() {
        mapper.findAll()?.forEach{ println(it)}
    }

    @Test
    fun test_04_findOne() {
        println(mapper.findOne(1))
    }
}
""".trim()
        val filePath = getFilePath(gjf, Generator.GenType.UT)
        writeFile(body, "${File(filePath).parent}/$clazzName.kt")
    }
}