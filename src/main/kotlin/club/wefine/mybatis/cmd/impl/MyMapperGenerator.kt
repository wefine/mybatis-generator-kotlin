package club.wefine.mybatis.cmd.impl

import club.wefine.mybatis.cmd.Generator
import club.wefine.mybatis.cmd.GeneratorKt
import org.mybatis.generator.api.GeneratedJavaFile
import org.slf4j.LoggerFactory
import java.io.File

class MyMapperGenerator(override var gjfs: List<GeneratedJavaFile>) : GeneratorKt {
    private val logger = LoggerFactory.getLogger(javaClass)

    override fun execute() {
        logger.info("generate mapper parent file...")
        if(gjfs.isNotEmpty()){
            singleWriter(gjfs[0])
        }
        logger.info("generate mapper parent file...Done!\n")
    }

    private fun singleWriter(gjf: GeneratedJavaFile) {
        val body = """
package ${gjf.targetPackage}

import org.springframework.transaction.annotation.Transactional

interface MyMapper<T> {
    fun findOne(id: Long): T?
    fun findAll(): List<T>?

    @Transactional
    fun insert(entity: T): Int

    @Transactional
    fun insertBatch(list: List<T>)

    @Transactional
    fun update(entity: T): Int

    @Transactional
    fun delete(id: Long): Int

    @Transactional
    fun updateWithoutNull(entity: T): Int

    @Transactional
    fun insertWithoutNull(entity: T): Int
}
""".trim()
        val filePath = getFilePath(gjf, Generator.GenType.KT)
        writeFile(body, "${File(filePath).parent}/MyMapper.kt")
    }
}